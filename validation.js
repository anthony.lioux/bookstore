const Joi = require('joi');

const registerValidation = (data) => {
const schema = Joi.object({
    name: Joi.string().min(6).required(),
    email: Joi.string().min(6).required(),
    password: Joi.string().min(6).required(),
});

return schema.validate(data)
};

const loginValidator = (data) => {
    const schema = Joi.object({
    email: Joi.string().min(6).required(),
    password: Joi.string().min(6).required(),
});
return schema.validate(data)

// const {error} = schema.validate(req.body);
// res.send(error.details[0].message);
// const newUser = new User(req.body);
// try {
//     const saveUser = await newUser.save();
//     res.send(saveUser); 
// } catch (err) {
//     res.status(400).send(err)
// }

}
module.exports = {registerValidation, loginValidator};