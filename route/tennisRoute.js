const express = require('express');
const router = express.Router();
const tournois = require('../data')


router.get('/rolland-garros',(req, res)=> {
    res.send(tournois.championnats["rolland-garros"]);
    });

router.post('/wimbledon',(req, res)=> {
    res.send(tournois.championnats["wimbledon"]);
    });

module.exports = router;