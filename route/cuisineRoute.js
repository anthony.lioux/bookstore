const cuisineRouter = require('express').Router();
const recipes = require('../data')

cuisineRouter.get('/all', (req, res) => {
    res.send("toutes mes recettes")
})

cuisineRouter.get('/pates', (req, res) => {
    res.send('les pates')
})

cuisineRouter.get('/lasagnes', (req, res) => {
    res.send(recipes.recettes["lasagnes"])
})

module.exports = cuisineRouter;