const booksRouter = require("express").Router();
const Book = require("../models/book");
const multer = require("multer");

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./public/images/");
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + "-" + file.originalname);
  },
});

const upload = multer({ storage: storage });
booksRouter.get("/", (req, res) => {
  Book.find({}, (err, books) => {
    if (err) throw err;
    res.render("books", { books: books });
  });
});

booksRouter.get("/new", (req, res) => {
  res.render("new");
});
booksRouter.post("/new", upload.single("cover"), (req, res) => {
  let newBook = new Book(req.body);
  newBook.cover = req.file.filename;
  newBook.save((err, book) => {
    if (err) throw err;
    res.redirect("/books");
  });
});

booksRouter.get("/edit/:book_id", (req, res) => {
  Book.findById({ _id: req.params.book_id }, (err, book) => {
    if (err) throw err;
    res.render("edit", { book: book });
  });
});
booksRouter.post("/edit/:book_id", (req, res) => {
  Book.findById({ _id: req.params.book_id }, (err, book) => {
    if (err) throw err;
    Object.assign(book, req.body).save((err, book) => {
      if (err) throw err;
      res.redirect("/books");
    });
  });
});
booksRouter.get("/:book_id", (req, res) => {
  Book.findById({ _id: req.params.book_id }, (err, book) => {
    if (err) throw err;
    res.render("book", { book: book });
  });
});
booksRouter.get("/delete/:book_id", (req, res) => {
  Book.remove({ _id: req.params.book_id }, (err) => {
    if (err) throw err;
    res.redirect("/books");
  });
});

module.exports = booksRouter;
