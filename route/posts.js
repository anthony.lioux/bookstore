const postRouter = require('express').Router();
const verify = require('../verifyToken');

postRouter.get('/',verify, (req, res) => {
res.json({
    posts: {
        title:"Mon premier post",
        description:"Random information",
    },
});
});

module.exports = postRouter;