const express = require("express");
const app = express();
const path = require("path");
const dotenv = require("dotenv");
dotenv.config();

const PORT = 4567;
const mongoose = require("mongoose");

// const uri = 'mongodb://localhost/foodtruck_db';

mongoose
  .connect(process.env.DB_CONNECT, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log(`[MONGODB is connected !]`))
  .catch((err) => console.log(Error, err.message));

const tennisRouter = require("./route/tennisRoute");
const cuisineRouter = require("./route/cuisineRoute");
const booksRouter = require("./route/booksRoute");
const userRouter = require("./route/auth");
const postRouter = require("./route/posts");
// const foodtruckRouter = require('./route/foodtruckRoute');

app.use(express.static("public"));
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get("/", (req, res) => {
  res.send("Welcome to the jungle");
});

app.get("/bonjour", (req, res) => {
  res.send("Welcome you");
});

// app.use('/foods', foodtruckRouter);
app.use("/books", booksRouter);
app.use("/tennis", tennisRouter);
app.use("/cuisine", cuisineRouter);
app.use("/users", userRouter);
app.use("/posts", postRouter);
app.listen(PORT, () => {
  console.log("Application is Ok!");
});
