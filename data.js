const informations = {
  championnats:{
    "rolland-garros":"Nadal",
    "wimbledon":"Djokovic",
    "Open d'Australie":"Djokovic"
  },
  recettes:{
    lasagnes:{
      ingredients:{
        "paquet de pâtes de lasagnes":1/4,
        "oignon jaune":3/4,
        "gousse d'ail":1/2,
        "viande hachée":"150g"
      }
    }
  }

}

module.exports = informations;