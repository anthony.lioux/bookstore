const jwt = require('jsonwebtoken')

auth=(req,res,next) => {
    // create & assign token
    const token = req.header("auth-token");
    next();
    if(!token) return res.status(400).send("Accès refusé !")
    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET)
        req.user = verified        
    } catch (err) {
        res.status(400).send("C'est un token éclaté au sol")
    }
};

module.exports = auth;